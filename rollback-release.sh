#!/bin/bash

set -e

# Variables
MASTER=master
DEV=develop
GIT=git
MAVEN=mvn
GREP=grep

# create master if local is not here
track_remote_branch(){

  local _BRANCH=$1
  if ! ${GIT} branch|${GREP} -wq ${_BRANCH}; then
    echo -n "Creating a local branch ${_BRANCH} to track origin/${_BRANCH}... "
    track_branch=$(${GIT} branch --track ${_BRANCH} origin/${_BRANCH} 2>&1)
    echo "Done"
  else
    echo "Branch ${_BRANCH} is already tracked."
  fi
}

revert_master(){

  local _BRANCH=${MASTER}
  echo "Checking out branch : ${_BRANCH}"
  _GIT_CO=$(${GIT} checkout ${_BRANCH} 2>&1)
  # get the last commitid
  _LAST_MERGE_CID=$(${GIT} log --pretty=format:"%h" -1)
  echo "Reverting last merge commit : ${GIT} revert -m1 ${_LAST_MERGE_CID}"
  #${GIT} revert -m1 ${_LAST_MERGE_CID}
}

revert_develop(){

  local _BRANCH=${DEV}
  echo "Checking out branch : ${_BRANCH}"
  _GIT_CO=$(${GIT} checkout ${_BRANCH} 2>&1)
  # get the last commitid
  _LAST_MERGE_CID=$(${GIT} log --pretty=format:"%h" -1)
  echo "Reverting last merge commit : ${GIT} revert -m1 ${_LAST_MERGE_CID}"
  #${GIT} revert -m1 ${_LAST_MERGE_CID}
}


# Processing
track_remote_branch ${MASTER}
track_remote_branch ${DEV}
revert_master
#revert_develop
#mvn release:rollback
#for tag in $(${GIT} tag -l|xargs); do
#  #${GIT} push --delete origin stdconf-1.0.2
#  ${GIT} tag -d $tag
#done


# rollback after complete releae
# maven create 2 commits ahead of origin and the last comes from the merge of release to master
#${GIT} checkout master
#${GIT} revert master~3..master

# same for develop 
#${GIT} checkout master
#${GIT} revert master~3..master

# delete tag (remote and local)
# push changes

# delete artifact from nexus
