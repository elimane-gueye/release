#Let's start doing release

#Step 1: Create a release branch from develop
git checkout -b release/1.0 develop

#Step 2: Do maven release

mvn release:prepare 

#There will be 2 commits happenings in your local git repo as part of prepare process

#Commit 1 # Change the pom version to release version (1.0.0 in our case) and append tag name (v1.0.0 in our case) in scm connection details of pom.xml. Commit the changed pom.xml

#Commit 2 # Change the pom version to next snapshot version ( 1.1.0-SNAPSHOT in our case) and remove the tag name from scm connection details. Commit the changed pom.xml so that things are ready for next development version

#Note: Between commit 1 and commit 2, a tag v1.0.0 will be created in your local git repo as well so that code that we are releasing is tagged. 

mvn release:perform 

# Based on tag created in release:prepare step, 1.0.0 version of application is built and deployed to central repository. 

#Step 3: Merge Release branch in Develop branch

git checkout develop             
git merge --no-ff release/1.0 

#Step 4: Merge Released code in master (in other word commit 1 of step 2)

git checkout master               
git merge --no-ff release/1.0~1 #Merge 1 commit before HEAD of release/1.0

#Step 5: Delete local release branch

git branch -D release/1.0

#Step 6: Push everything to remote (Tag/Master and Develop)

git push --all && git push --tags


